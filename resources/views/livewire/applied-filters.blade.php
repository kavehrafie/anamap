<div class="row" x-data>
    @forelse($filters as $key => $filter)
        @if ($key === 'years')
            <div class="badge badge--dark badge--flex mb-small">
                <span class="my-auto">{{implode(' - ', $filter)}}</span>
                <button class="button button--close"
                        @click="$wire.resetYears(); $dispatch('reset-slider')"
                >
                    <x-icon name="close" :size="25" color="white"/>
                </button>
            </div>
        @endif
    @empty
    @endforelse
</div>
