<x-page-layout>
    <x-slot name="aside">
        <x-accardion title="Date">
            <div x-data="{years: @entangle('selectedYears'), custom: ''}"
                 x-init="$refs.custom.checked = true;"
                 x-on:slider-updated.window="custom = $event.detail.join(' - ');
                 years = $event.detail; $refs.custom.checked = true;"
            >
                @foreach ($intervals as $key => $value)
                    <div class="radio">
                        <input id="{{$key}}" name="radio" type="radio" value="{{json_encode($value)}}"
                               @click="years = {{json_encode($value)}}; custom = '';"
                               wire:loading.attr="disabled"
                        >
                        <label for="{{$key}}">{{$key}}</label>
                    </div>
                @endforeach
                <div x-show.transition="custom.length > 0" class="radio">
                    <input x-ref="custom" id="custom" name="radio" type="radio">
                    <label for="custom"><span x-text="custom"></span></label>
                </div>
                <x-slider :start="$selectedYears[0] ?? $min" :end="$selectedYears[1] ?? $max" :min="$min" :max="$max"/>
            </div>
        </x-accardion>
        <x-accardion title="Medium">
            <div>
                @forelse ($types as $key => $medium)
                    @if(is_array($medium))
                        <div class="checkbox">
                            <input id="{{$key}}" name="{{$key}}" type="checkbox"
                                   value="{{$key}}"
                                   wire:loading.attr="disabled"
                                   wire:model="selectedMediasParents"
                                   wire:click="selectMediasParents('{{$key}}')">
                            <label for="{{$key}}">{{ ucfirst($key) }}</label>
                        </div>
                    <div class="pl">

                        @foreach($medium as $child)
                            <div class="checkbox">
                                <input id="{{$child}}" name="{{$child}}" type="checkbox"
                                       value="{{$child}}"
                                       wire:loading.attr="disabled"
                                       wire:click="selectMediasChild('{{$key}}')"
                                       wire:model="selectedMedias">
                                <label for="{{$child}}">{{ ucfirst($child) }}</label>
                            </div>
                        @endforeach
                    </div>
                    @else
                        <div class="checkbox">
                            <input id="{{$medium}}" name="{{$medium}}" type="checkbox"
                                   value="{{$medium}}"
                                   wire:loading.attr="disabled"
                                   wire:model="selectedMedias">
                            <label for="{{$medium}}">{{ ucfirst($medium) }}</label>
                        </div>
                    @endif
                @empty
                @endforelse
            </div>
        </x-accardion>
        <x-accardion title="Languages">
            <div>
                @forelse ($languages as $key => $value)
                    <div class="checkbox">
                        <input id="{{$value}}" name="{{$value}}" type="checkbox"
                               value="{{$value}}"
                               wire:loading.attr="disabled"
                               wire:model="selectedLanguages">
                        <label for="{{$value}}">{{ ucfirst($value) }}</label>
                    </div>
                @empty
                @endforelse
            </div>
        </x-accardion>
        <x-accardion title="Countries">
            <div>
                @forelse ($countries as $key => $value)
                    <div class="checkbox">
                        <input id="{{$value}}" name="{{$value}}" type="checkbox"
                               value="{{$value}}"
                               wire:loading.attr="disabled"
                               wire:model="selectedCountries">
                        <label for="{{$value}}">{{ ucfirst($value) }}</label>
                    </div>
                @empty
                @endforelse
            </div>
        </x-accardion>

    </x-slot>

    <div class="loadingBar" wire:loading>
    </div>

    <!-- filter tags -->
    <div class="row">

        <!-- filter years tag -->
        @if ($selectedYears)
            <div class="col-sm">
                <div class="row bg-gray-light p-small">
                    <span class="badge badge--small my-auto">Between</span>
                    <div class="badge badge--dark badge--flex my-auto">
                        <span class="my-auto">{{implode(' - ', $selectedYears)}}</span>
                        <button class="button button--close"
                                wire:click="resetYears()"
                                wire:loading.attr="disabled"
                        >
                            <x-icon name="close" :size="25" color="white"/>
                        </button>
                    </div>
                </div>
            </div>
        @endIf

    <!-- filter countries tag -->
        @if (count($selectedCountries))
            <div class="col">
                <div class="row bg-gray-light p-small">
                    <span class="badge badge--small my-auto">Countries</span>
                    @forelse($selectedCountries as $key => $value)
                        <div class="badge badge--dark badge--small badge--flex my-auto">
                            <span class="my-auto">{{ucfirst($value)}}</span>
                            <button class="button button--close"
                                    wire:click="dropCountry({{$key}})"
                                    wire:loading.attr="disabled"
                            >
                                <x-icon name="close" :size="25" color="white"/>
                            </button>
                        </div>
                    @empty
                    @endforelse
                </div>
            </div>
        @endif

    <!-- filter media tag -->
        @if (count($selectedMedias))
            <div class="col">
                <div class="row bg-gray-light p-small">
                    <span class="badge badge--small my-auto">Media</span>
                    @forelse($selectedMedias as $key => $value)
                        <div class="badge badge--dark badge--small badge--flex my-auto">
                            <span class="my-auto">{{ucfirst($value)}}</span>
                            <button class="button button--close"
                                    wire:click="dropMedia({{$key}})"
                                    wire:loading.attr="disabled"
                            >
                                <x-icon name="close" :size="25" color="white"/>
                            </button>
                        </div>
                    @empty
                    @endforelse
                </div>
            </div>
        @endif

    <!-- filter language tag -->
        @if (count($selectedLanguages))
            <div class="col">
                <div class="row bg-gray-light p-small">
                    <span class="badge badge--small my-auto">Languages</span>
                    @forelse($selectedLanguages as $key => $value)
                        <div class="badge badge--dark badge--flex my-auto">
                            <span class="my-auto">{{ucfirst($value)}}</span>
                            <button class="button button--close"
                                    wire:click="dropLanguage({{$key}})"
                                    wire:loading.attr="disabled"
                            >
                                <x-icon name="close" :size="25" color="white"/>
                            </button>
                        </div>
                    @empty
                    @endforelse
                </div>
            </div>
        @endif
    </div>


    <div class="mosaics" x-ref="mosaics" x-data="mosaics()"
         :style="`height: ${getHeight()}px;`  "
    >
        @php
            $thumbWidth = 350
        @endphp

        @forelse($pops as $pop)
            <div class="mosaics__cell">
                <!-- Card Image -->
                @if ($pop->hasImage('cover'))
                    @php
                        $image = $pop->imageAsArray('cover', 'free', ['w' => $thumbWidth]);
                        if (empty($image)) {
                            $image = $pop->imageAsArray('cover', 'default', ['w' => $thumbWidth]);
                        }
                    @endphp

                    <img width="{{$thumbWidth}}"
                         height="{{$thumbWidth / $image['width'] * $image['height'] }}"
                         src="{{$image['src']}}">

                @else
                    <div class="img-placeholder">
                        <x-icon/>
                    </div>
            @endif
            <!-- Card Heading -->
                <div class="mosaic__detail">
                    <h3 class="text-big">{{$pop['title']}}</h3>
                    <p>{{$pop['start_year']}}</p>
                </div>
            </div>
        @empty
            No entries!
        @endforelse
    </div>


    @push('script')
        <script>
            /**
             * this function determine the height of mosaics container Livewire component
             * @returns getHeight() |number
             */
            function mosaics() {
                return {
                    getHeight() {
                        const children = this.$refs.mosaics.children;
                        let heights = []
                        for (let child of children) {
                            let style = getComputedStyle(child)
                            heights.push(parseInt(style.height) + parseInt(style.marginRight) * 4)
                        }
                        let b = []
                        let c = []
                        for (let i = 0; i < heights.length; i++) {
                            b.push(heights[i])
                            if (!((i + 1) % 3) || i == heights.length - 1) {
                                c.push(b)
                                b = []
                            }
                        }

                        return c.length && c.reduce((acc, curr) => parseInt(acc) + Math.max(...curr), 0);
                    }
                }

            }
        </script>
    @endpush
</x-page-layout>
