<div>
    <div class="loadingBar" wire:loading>
    </div>
<div class="mosaics" x-ref="mosaics" x-data="mosaics()"
     :style="`height: ${getHeight()}px;`  "
>
    @php
        $thumbWidth = 350
    @endphp

    @forelse($pops as $pop)
        <div class="mosaics__cell">
            <!-- Card Image -->
            @if ($pop->hasImage('cover'))
                @php
                    $image = $pop->imageAsArray('cover', 'free', ['w' => $thumbWidth]);
                    if (empty($image)) {
                        $image = $pop->imageAsArray('cover', 'default', ['w' => $thumbWidth]);
                    }
                @endphp

                <img width="{{$thumbWidth}}"
                     height="{{$thumbWidth / $image['width'] * $image['height'] }}"
                     src="{{$image['src']}}">

            @else
                <div class="img-placeholder">
                    <x-icon/>
                </div>
        @endif
        <!-- Card Heading -->
            <div class="mosaic__detail">
                <h3 class="text-big">{{$pop['title']}}</h3>
                <p>{{$pop['start_year']}}</p>
            </div>
        </div>
    @empty
        No entries!
    @endforelse
</div>
</div>
