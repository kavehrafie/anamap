<x-app-layout>
    <div class="container-medium">
        <main>
            <div class="masonry">
                @forelse($articles as $article)
                    <a class="item @if($article->featured) item--featured @endif"
                       href="{{route('blogs.show', $article->slug)}}">
                        <div class="image-container"><img src="{{$article->image('cover')}}"></div>
                        <p class="text-center">
                            {{$article->updated_at->diffForHumans() ?? $article->created_at->diffForHumans()}}
                        </p>
                        <h2 class="text-huge text-center">{{$article->title}}</h2>
                        <p class="text-center px">{{$article->abstract}}</p>
                    </a>
                @empty
                @endforelse
            </div>
        </main>
    </div>
</x-app-layout>
