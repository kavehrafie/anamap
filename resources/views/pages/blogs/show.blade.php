<x-app-layout>
    <div class="container-medium">
        <main>
            <article class="main">
                <h2 class="text-huge text-withSubtitle">
                    {{ucfirst($article->title)}}
                </h2>
                <p>Posted on {{$article->created_at->diffForHumans()}}</p>
                <div class="content">{!! $article->renderBlocks() !!}</div>
            </article>
            <section>
                <div class="blog-nav row border-top">
                    @if($next)
                    <div class="blog-nav__item col border-right">
                        <a class="row" href="{{$next->slug}}">
                            <div class="col aligner aligner--contentStart arrow-to-left"><x-icon name="arrow-left"></x-icon></div>
                            <div class="col aligner aligner--centerVertical aligner--contentEnd">{{$next->title}}</div>
                        </a>
                    </div>
                    @endif
                    @if($previous)
                    <div class="blog-nav__item col">
                        <a class="row" href="{{$previous->slug}}">
                            <div class="col aligner aligner--centerVertical">{{$previous->title}}</div>
                            <div class="col aligner aligner--contentEnd arrow-to-right"><x-icon name="arrow-right"></x-icon></div>
                        </a>
                    </div>
                    @endif
                </div>
            </section>
        </main>
    </div>
</x-app-layout>
