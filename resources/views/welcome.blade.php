<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{config('app.name')}}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }
            .container-welcome {
                height: 100vh;
                width: 100vw;
                display: flex;
                justify-content: center;
                align-items: center;
            }

            .p4 {
                padding: 3em;
            }
        </style>
        <!-- Scripts -->
        <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.8.1/dist/alpine.js" defer></script>
    </head>
    <body>
        <x-app-layout>
                <p>Welcome to</p>
            <div class="container-welcome">
                <ul>
                    <li>Landing page</li>
                    <li>Blog index page</li>
                    <li>Blog show page</li>
                    <li>Pop index page</li>
                    <li>Pop show page</li>
                </ul>
                <div class="p4">
                    <h1>{{config('app.name')}}</h1>
                    <p>Login the Admin page <a href="{{route('admin.login.form')}}">here</a>.</p>
                    <p>or <a href="{{route('pop-cultures.index')}}">check out the sample dataset table.</a></p>
                </div>
            </div>
        </x-app-layout>
    </body>
</html>
