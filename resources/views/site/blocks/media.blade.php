@php
    $image = $block->imageAsArray('image', 'desktop');
@endphp
<div
    class="{{$block->input('size')}}"
>
    <img src="{{$image['src']}}" >
    <p class="text-gray">{{$image['caption']}}</p>
</div>
