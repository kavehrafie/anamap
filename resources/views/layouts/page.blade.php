<div class="container-medium" x-data="{show: true}">
    <div class="row">
        <div class="col">
            <button class="button button--control row" x-on:click="show = !show">
                <x-icon name="controls" size="30"/>
                <span class="my-auto">Show Filters</span>
            </button>
        </div>
        <div class="col--third">
            <div class="formCollapsed">
                <div class="input formCollapsed-item formCollapsed-itemPrimary">
                    <input id="test8" placeholder="Search" type="text">
                </div>
                <button class="formCollapsed-item button button--secondary">
                    Go
                </button>
            </div>
        </div>
    </div>
    <div class="row">
        <aside x-show.transition="show" class="col-sm-3">
            {{ $aside ?? '' }}
        </aside>
        <main class="col pl">
            {{$slot}}
        </main>
    </div>
</div>

