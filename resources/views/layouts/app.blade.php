<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{$title ?? config('app.name', 'Laravel')}}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    @livewireStyles

    <!-- JavaScript -->
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.8.1/dist/alpine.js" defer></script>
    <script src="{{asset('js/app.js')}}" defer></script>
</head>
<body {{ $attributes->merge(['class' => "page "])}} >
    <header class="border-bottom mb-medium ">
        <x-header />
    </header>

    {{$slot}}

    <footer class="border-top">
        <x-footer />
    </footer>

    @stack('script')

    @livewireScripts
</body>
</html>
