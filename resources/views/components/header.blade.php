<div class="header">
    <div class="header__title">
        <h1>Ancient Americas, Appropriated</h1>
        <span class="subtitle">Modern Representations of the Pre-Columbian Past</span>
    </div>
    <div class="header__nav">
        @forelse($menuItems as $item)
            <a class="{{\Illuminate\Support\Facades\Route::currentRouteName() === $item['route'] ? 'active' : ''}}" href="{{route($item['route'])}}">{{$item['title']}}</a>
        @empty
        @endforelse
    </div>
</div>
