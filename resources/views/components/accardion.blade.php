@props([
    'title' => '',
])
<div class="accordion" x-data="{show: false}" x-cloak>

    <button class="accordion__title"
            :class="{'accordion--opened': show}"
            @click="show = !show">
        {{$title}}
    </button>

    <div class="accordion__content" x-show.transition.origin.top="show">
        {{$slot}}
    </div>
</div>
