@props(['name' => 'image', 'size' => 48, 'color' => ''])

@if ($name === 'image')
    <svg role="img"
         xmlns="http://www.w3.org/2000/svg"
         width="{{$size}}" height="{{$size}}"
         viewBox="0 0 24 24"
         aria-labelledby="imageIconTitle"
         stroke="{{$color}}"
         stroke-width="1" stroke-linecap="square" stroke-linejoin="miter"
         fill="none"><title id="imageIconTitle">Image</title>
        <rect width="18" height="18" x="3" y="3"/>
        <path stroke-linecap="round" d="M3 14l4-4 11 11"/>
        <circle cx="13.5" cy="7.5" r="2.5"/>
        <path stroke-linecap="round" d="M13.5 16.5L21 9"/>
    </svg>

@elseif($name === 'close')
    <svg role="img"
         xmlns="http://www.w3.org/2000/svg"
         width="{{$size}}" height="{{$size}}"
         viewBox="0 0 24 24"
         aria-labelledby="closeIconTitle"
         stroke="{{$color}}"
         stroke-width="1" stroke-linecap="square"
         stroke-linejoin="miter" fill="none"><title id="closeIconTitle">Close</title>
        <path d="M6.34314575 6.34314575L17.6568542 17.6568542M6.34314575 17.6568542L17.6568542 6.34314575"/>
    </svg>


@elseif($name === 'arrow-left')
    <svg xmlns="http://www.w3.org/2000/svg"
         role="img"
         width="{{$size}}" height="{{$size}}"
         viewBox="0 0 24 24"
         aria-labelledby="arrowLeftIconTitle" stroke="{{$color}}"
         stroke-width="1" stroke-linecap="square"
         stroke-linejoin="miter"
         fill="none"><title id="arrowLeftIconTitle">Arrow Left</title>
        <path d="M9 6l-6 6 6 6"/>
        <path d="M21 12H4"/>
        <path stroke-linecap="round" d="M3 12h1"/>
    </svg>

@elseif($name === 'arrow-right')
    <svg xmlns="http://www.w3.org/2000/svg" role="img"
         width="{{$size}}" height="{{$size}}"
         viewBox="0 0 24 24"
         aria-labelledby="arrowRightIconTitle"
         stroke="{{$color}}" stroke-width="1" stroke-linecap="square"
         stroke-linejoin="miter" fill="none"><title id="arrowRightIconTitle">Arrow Right</title>
        <path d="M15 18l6-6-6-6"/>
        <path d="M3 12h17"/>
        <path stroke-linecap="round" d="M21 12h-1"/>
    </svg>

@elseif($name === 'controls')
    <svg xmlns="http://www.w3.org/2000/svg"
         width="{{$size}}" height="{{$size}}"
         viewBox="0 0 24 24" fill="none"
         aria-labelledby="controlsAltIconTitle"
         stroke="{{$color}}" stroke-width="1" stroke-linecap="square"
         stroke-linejoin="miter" ><title id="controlsAltIconTitle">Controls</title>
        <circle cx="9" cy="6" r="2"/>
        <path d="M4 6H7"/>
        <path d="M11 6H20"/>
        <circle cx="9" cy="18" r="2"/>
        <path d="M4 18H7"/>
        <path d="M11 18H20"/>
        <circle cx="15" cy="12" r="2"/>
        <path d="M4 12H13"/>
        <path d="M17 12L20 12"/>
    </svg>
@endif
