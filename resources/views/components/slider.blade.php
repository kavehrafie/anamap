@props([ 'label' => '', 'min' => 0, 'max' => 100, 'start' => 0, 'end' => 100])
<div class="slider" role='group' aria-labelledby='multi-lbl'
     style='--start: {{$start}}; --end: {{$end}}; --min: {{$min}}; --max: {{$max}}'
     x-data="{start: {{$start}}, end:{{$end}}}"
     x-init="        $watch('start', value => {
            $refs.start.parentNode.style.setProperty('--start', start);
            });
        $watch('end', value => {
            $refs.start.parentNode.style.setProperty('--end', end);
            });"
     x-on:sync-slider.window="start = $event.detail[0]; end = $event.detail[1]; console.log($event.detail);"
>
    @if ($label)
        <div id='multi-lbl'>Multi thumb slider:</div>
    @endif
    <label class='sr-only' for='start-range'>Value A:</label>
    <input x-ref="start" type="range" id="start-range" min='{{$min}}'  max='{{$max}}' x-model="start"
        @click="$dispatch('slider-updated', [parseInt(start), parseInt(end)])"
    >
    <label x-ref="end" class='sr-only' for='end-range'>Value B:</label>
    <input type="range" id="end-range" min='{{$min}}' max='{{$max}}' x-model="end"
        @click="$dispatch('slider-updated', [parseInt(start), parseInt(end)])"
    >
</div>

