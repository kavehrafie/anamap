@extends('twill::layouts.form', [
        'additionalFieldsets' => [
        ['fieldset' => 'creators', 'label' => 'Creators'],
        ['fieldset' => 'images', 'label' => 'Images'],
        ['fieldset' => 'metadata', 'label' => 'Metadata'],
        ['fieldset' => 'extras', 'label' => 'Extras'],
    ]
])

@section('contentFields')

    @formField('input', [
        'name' => 'title',
        'label' => 'Title',
        'placeholder' => 'Title',
        'note' => 'This is the title of the cultural entry.',
        'required' => true,
    ])

    @formField('select', [
        'name' => 'media_type',
        'label' => 'Media type',
        'placeholder' => 'Select an media type',
        'note' => 'The the media type.',
        'required' => true,
        'options' => $mediaOptions
    ])

    @formField('input', [
        'name' => 'title_of_series',
        'label' => 'Title of Series',
        'placeholder' => 'E.g. Doctor Who',
        'note' => 'The titleof series if any exists.',
    ])


    @component('twill::partials.form.utils._connected_fields', [
            'fieldName'=>'title_of_series',
            'fieldValues'=> '',
            'isEqual' => false
    ])
        @formField('input', [
            'name' => 'number_in_series',
            'label' => 'Number in Series',
            'placeholder' => 'Number in Series',
            'note' => 'The series number if it exists.',
        ])
    @endcomponent

    @formField('input', [
        'name' => 'publisher',
        'label' => 'Publisher',
        'placeholder' => 'E.g. Heinz Karl Heiland-Film',
        'note' => 'Enter the publisher in this field.',
    ])

    @formField('select', [
        'name' => 'start_year',
        'label' => 'Start Year',
        'placeholder' => 'Select an starting year',
        'note' => 'The year that this entry was created.',
        'required' => true,
        'options' => $yearOptions
    ])

    @formField('select', [
        'name' => 'end_year',
        'label' => 'Ending Year',
        'placeholder' => 'Select an ending year',
        'note' => 'If this entry ended on the same year that it started, leave this field blank.',
        'options' => $yearOptions
    ])

    @formField('wysiwyg', [
        'name' => 'description',
        'label' => 'Description',
        'toolbarOptions' => [ [ 'header' => [2, 3, false] ], 'list-ordered', 'list-unordered', [ 'indent' => '-1'], [ 'indent' => '+1' ] ],
        'placeholder' => 'E.g. This film was the first...',
        'maxlength' => 400,
        'note' => 'Description of the cultural entry goes here.',
    ])

    @formField('browser', [
        'label' => 'Languages',
        'max' => 4,
        'name' => 'languages',
        'moduleName' => 'languages',
    ])

    @formField('browser', [
        'label' => 'Countries',
        'max' => 4,
        'name' => 'countries',
        'moduleName' => 'countries'
    ])


@stop

@section('fieldsets')
    <a17-fieldset title="Creators" id="creators" :open="true">
        @formField('repeater', ['type' => 'people'])
    </a17-fieldset>
    <a17-fieldset title="Images" id="images" :open="true">
        @formField('medias', [
            'name' => 'cover',
            'label' => 'Cover image',
            'note' => 'Also used in listings',
            'fieldNote' => 'This image will be used for cover image.'
        ])

        @formField('medias', [
            'name' => 'slides',
            'label' => 'Slides',
            'max' => 5,
            'fieldNote' => 'Minimum image width: 500px'
        ])
    </a17-fieldset>
    <a17-fieldset title="Metadata" id="metadata" :open="true">
        @formField('tags')
        @formField('input', [
            'name' => 'notes',
            'label' => 'Notes',
            'type' => 'textarea',
            'rows' => 3,
            'placeholder' => 'E.g. check the dates ...',
            'note' => 'This field is intended for internal use and it will not be rendered in front-end.',
        ])
    </a17-fieldset>
    <a17-fieldset title="Alternative Fields" id="extras" :open="true">
        @formField('repeater', ['type' => 'extra'])
    </a17-fieldset>
@stop
