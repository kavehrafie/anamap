@extends('twill::layouts.form')

@section('contentFields')
    @formField('input', [
        'name' => 'first_name',
        'label' => 'First name',
        'maxlength' => 100
    ])
    @formField('input', [
        'name' => 'last_name',
        'label' => 'Last name',
        'maxlength' => 100
    ])
@stop
