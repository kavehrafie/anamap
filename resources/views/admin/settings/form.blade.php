@extends('twill::layouts.settings')

@section('contentFields')
    @formField('input', [
        'label' => 'Year start',
        'type' => 'number',
        'name' => 'year_start',
        'default' => 1900
    ])
    @formField('input', [
        'label' => 'Media types',
        'name' => 'media_types',
        'default' => 'film',
        'note' => 'Use this field to set the available media types. Enter each entries following with comma ","'
    ])
    @formField('input', [
        'label' => 'Creator roles',
        'name' => 'creator_roles',
        'note' => 'Use this field to set the available creator roles. Enter each entries following with comma ", "'
    ])
@stop
