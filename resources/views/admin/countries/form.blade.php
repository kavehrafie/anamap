@extends('twill::layouts.form', [
    'publish'=> false,
    'update' => false
])

@section('contentFields')
    @formField('input', [
    'name' => 'code',
    'label' => 'Code',
    'maxlength' => 100,
    'readonly' => true,
    ])
    @formField('input', [
    'name' => 'name',
    'label' => 'Name',
    'maxlength' => 100,
    'readonly' => true,
    ])
@stop
