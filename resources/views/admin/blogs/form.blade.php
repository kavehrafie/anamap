
@extends('twill::layouts.form', [
    'additionalFieldsets' => [
        ['fieldset' => 'pop_cultures', 'label' => 'Pop Cultures'],
    ]
])

@section('contentFields')
    @formField('medias', [
        'name' => 'cover',
        'label' => 'Cover image',
        'note' => 'Also used in listings',
        'fieldNote' => 'This image will be used for cover image.',
        'max' => 1
    ])

    @formField('input', [
        'name' => 'abstract',
        'label' => 'Abstract',
        'maxlength' => 200
    ])

    @formField('block_editor', [
        'blocks' => ['text', 'media', 'quote', 'footnote']
    ])

@stop

@section('fieldsets')
    @formFieldset(['id' => 'pop_cultures', 'title' => 'Pop Cultures'])
        @formField('browser', [
        'label' => 'Popular Culture Dataset',
        'name' => 'popCultures',
        'max' => 30,
        'moduleName' => 'popCultures'
        ])
    @endformFieldset
@stop
