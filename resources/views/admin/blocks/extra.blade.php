@php
    $options = [
        [
            'value' => 'title',
            'label' => 'Title'
        ],
        [
            'value' => 'title_of_series',
            'label' => 'Title of Series'
        ],
        [
            'value' => 'number_in_series',
            'label' => 'Number in Series'
        ],
        [
            'value' => 'language',
            'label' => 'Language'
        ],
        [
            'value' => 'publisher',
            'lable' => 'Publisher'
        ]
    ];
@endphp

@formField('input', [
'name' => 'value',
'label' => 'Value'
])

@formField('select', [
    'name' => 'field',
    'label' => 'Field',
    'placeholder' => 'Select a field',
    'default' => 'title',
    'options' => $options
])
