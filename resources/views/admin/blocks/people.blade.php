@php
    $settings = app(\A17\Twill\Repositories\SettingRepository::class)
        ->byKey('creator_roles') ?? 'director, artist, writer';

    $options = array_map(function($value) {
        return [
            'value' => strtolower($value),
            'label' => ucfirst($value)
        ];
        },explode(', ',$settings));
@endphp

@formField('input', [
'name' => 'first_name',
'label' => 'First Name'
])

@formField('input', [
'name' => 'last_name',
'label' => 'Last Name'
])

@formField('select', [
    'name' => 'role',
    'label' => 'Role',
    'placeholder' => 'Select an office',
    'options' => $options
])
