@formField('select', [
    'name' => 'size',
    'label' => 'Size',
    'unpack' => true,
    'note'  => 'Specifies how many columns the media occupies',
    'default' => 'col-span-1',
    'options' => [
        [
        'value' => 'col-span-2',
        'label' => 'Full'
        ],
        [
        'value' => 'col-span-1',
        'label' => 'Half'
        ],
    ]
])

@formField('medias', [
    'name' => 'image',
    'label' => 'Images',
    'withVideoUrl' => false,
    'max' => 1,
])

@formField('input', [
    'name'  => 'video',
    'label' => 'Video url',
    'note'  => 'Video will overwrite previously selected images',
])

