<?php

return [
    'enabled' => [
        'users-management' => true,
        'media-library' => true,
        'file-library' => true,
        'block-editor' => true,
        'settings' => true,
        'dashboard' => true,
        'search' => true,
        'activitylog' => true,
    ],
    'block_editor' => [
        'repeaters' => [
            'people' => [
                'title' => 'Creator',
                'trigger' => 'Add creator',
                'component' => 'a17-block-people'
            ],
            'extra' => [
                'title' => 'Extra',
                'trigger' => 'Add extra fields',
                'component' => 'a17-block-extra'
            ]
        ],
        'blocks' => [
            'media' => [
                'title' => 'Media',
                'icon' => 'image',
                'component' => 'a17-block-media',
            ],
        ]
    ],
    'dashboard' => [
        'modules' => [
            'App\Models\Blog' => [ // module name if you added a morph map entry for it, otherwise FQCN of the model (eg. App\Models\Project)
                'name' => 'blogs', // module name
                'label' => 'Blogger', // optional, if the name of your module above does not work as a label
                'label_singular' => 'blog', // optional, if the automated singular version of your name/label above does not work as a label
//                'routePrefix' => 'work', // optional, if the module is living under a specific routes group
                'count' => true, // show total count with link to index of this module
                'create' => true, // show link in create new dropdown
                'activity' => true, // show activities on this module in actities list
                'draft' => true, // show drafts of this module for current user
                'search' => true, // show results for this module in global search
            ],
            'App\Models\PopCulture' => [ // module name if you added a morph map entry for it, otherwise FQCN of the model (eg. App\Models\Project)
                'name' => 'popCultures', // module name
                'label' => 'Pop Culture Dataset', // optional, if the name of your module above does not work as a label
                'label_singular' => 'popCulture', // optional, if the automated singular version of your name/label above does not work as a label
//                'routePrefix' => 'work', // optional, if the module is living under a specific routes group
                'count' => true, // show total count with link to index of this module
//                'create' => true, // show link in create new dropdown
                'activity' => true, // show activities on this module in actities list
//                'draft' => true, // show drafts of this module for current user
                'search' => true, // show results for this module in global search
            ],
        ],
    ],
];
