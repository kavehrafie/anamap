<?php

return [
    'popCultures' => [
        'title' => 'Popular Culture References',
        'module' => true,
    ],
    'blogs' => [
        'title' => 'Blogger',
        'module' => true,
    ],
    'settings' => [
        'title' => 'Settings',
        'route' => 'admin.settings',
        'params' => ['section' => 'form'],
        'primary_navigation' => [
            'form' => [
                'title' => 'Form Defaults',
                'route' => 'admin.settings',
                'params' => ['section' => 'form']
            ],
        ]
    ],
];
