<?php

// Register Twill routes here (eg. Route::module('posts'))

Route::module('popCultures');
Route::module('creators');
Route::module('countries');
Route::module('languages');
Route::module('blogs');
