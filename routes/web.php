<?php

use App\Http\Livewire\PopIndex;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/about', function () {
    return view('pages.about');
})->name('about');

Route::get('/articles', 'BlogController@index')->name('blogs.index');
Route::get('/articles/{article}', 'BlogController@show')->name('blogs.show');
Route::get('/datasets', 'PopCultureFController@index')->name('pop-cultures.index');

