<?php

use App\Models\Country;
use App\Models\Language;
use App\Models\PopCulture;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Log;

class PopCultureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // file path
        $path = storage_path('app/public/' . 'pops.csv');

        $file_handle = fopen($path, 'r');

        while (!feof($file_handle)) {

            $line = fgetcsv($file_handle);
            if (empty($line)) {
                continue; // skip blank lines
            }
            if ($line[0] == 'Year') {
                continue; // skip column headers
            }
            if ($line[0] == '') {
                break; // exit the loop when reach to the end of the file
            }
            /*
            CSV column names
            0 ... ID
            1 ... NAME
            2 ... NUMBER
            3 ... NOTES
            */

            $insert = array();

            $insert['start_year'] = $line[0];
            $insert['media_type'] = strtolower($line[1]);
            $insert['title'] = $line[4];
            $insert['title_of_series'] = $line[5];
            $insert['publisher'] = $line[8];
            $insert['notes'] = $line[9];
            $model = PopCulture::create($insert);
            $countries = [];
            $languages = [];

//            Log::info(print_r( $line, true));
            $position = 0;
            foreach(Country::whereIn('name', explode('/', $line[2]))->get()->pluck('id') as $id) {
                $countries[$id] = ['position' => $position++];
            }

            $position = 0;
            foreach(Language::whereIn('name', explode('/', $line[3]))->get()->pluck('id') as $id) {
                $languages[$id] = ['position' => $position++];
            }

            $model->countries()->attach($countries);
            $model->languages()->attach($languages);

        }


        fclose($file_handle);

    }
}
