<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLanguagesTables extends Migration
{
    public function up()
    {
        Schema::create('languages', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table, false, false);

            $table->string('code', 2)
                ->index();
            $table->string('name', 75);
        });


        Schema::create('language_pop_culture', function(Blueprint $table) {
            createDefaultRelationshipTableFields($table, 'language', 'pop_culture');
            $table->integer('position')->unsigned()->index();
        });

    }


    public function down()
    {

        Schema::dropIfExists('language_pop_culture');
        Schema::dropIfExists('languages');
    }
}
