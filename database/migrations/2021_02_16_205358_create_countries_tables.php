<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCountriesTables extends Migration
{
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table, false, false);
            $table->string('code', 2)
                ->index();
            $table->string('name', 75);

        });

        Schema::create('country_pop_culture', function(Blueprint $table) {
            createDefaultRelationshipTableFields($table, 'country', 'pop_culture');
            $table->integer('position')->unsigned()->index();
        });

    }

    public function down()
    {

        Schema::dropIfExists('country_pop_culture');
        Schema::dropIfExists('countries');
    }
}
