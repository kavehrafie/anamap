<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleBlocks;
use A17\Twill\Repositories\Behaviors\HandleSlugs;
use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\Behaviors\HandleRevisions;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\Blog;
use Illuminate\Support\Facades\Log;

class BlogRepository extends ModuleRepository
{
    use HandleBlocks, HandleSlugs, HandleMedias, HandleRevisions;

    public function __construct(Blog $model)
    {
        $this->model = $model;
    }

    public function getFormFields($object)
    {
        $fields =  parent::getFormFields($object); // TODO: Change the autogenerated stub
        // get fields for a browser
        $fields['browsers']['popCultures'] = $this->getFormFieldsForBrowser($object, 'popCultures');

        return $fields;
    }
    public function afterSave($object, $fields)
    {

        // Save PopCulture relationships
        $this->updateBrowser($object, $fields, 'popCultures');

        parent::afterSave($object, $fields); // TODO: Change the autogenerated stub
    }

    public function next($position)
    {
        return $this->model->where('position', '>', $position)
            ->oldest('id')
            ->first();
    }

    public function previous($position)
    {
        return $this->model->where('position', '<', $position)
            ->latest('id')
            ->first();
    }
}
