<?php

namespace App\Repositories;

use A17\Twill\Models\Model;
use A17\Twill\Repositories\Behaviors\HandleBlocks;
use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\Behaviors\HandleRevisions;
use A17\Twill\Repositories\Behaviors\HandleSlugs;
use A17\Twill\Repositories\Behaviors\HandleTags;
use A17\Twill\Repositories\ModuleRepository;
use A17\Twill\Repositories\SettingRepository;
use A17\Twill\Services\Blocks\BlockCollection;
use App\Models\PopCulture;
use App\Models\PopCultureExtra;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class PopCultureRepository extends ModuleRepository
{
    use HandleBlocks, HandleSlugs, HandleMedias, HandleRevisions, HandleTags;


    public function __construct(PopCulture $model)
    {
        $this->model = $model;
    }


    // implement the getFormFields method
    public function getFormFields($object)
    {
        // don't forget to call the parent getFormFields function
        $fields = parent::getFormFields($object);

        $fields['browsers']['countries'] = $this->getFormFieldsForBrowser(
            $object,
            'countries',
            null,
            'name');
        $fields['browsers']['languages'] = $this->getFormFieldsForBrowser(
            $object,
            'languages',
            null,
            'name');

        // get fields for people repeater
        $fields = $this->getFormFieldsForRepeater($object, $fields, 'people', 'Creator');
         foreach ($object->people as $person) {
            $fields['repeaterFields']['people'][] = [
                'name' => "blocks[people-{$person->id}][role]",
                'value' => $person->pivot->role
            ];
//            Log::info(print_r($fields['repeaterFields']['people'], true));
         }


        $repeatersList = app(BlockCollection::class)->getRepeaterList()->keyBy('name');

        $repeaters = [];
        $repeatersFields = [];
        foreach ($object->extras as $extra) {
            $repeatersFields[] = [
                  'name'  => "blocks[extra-{$extra->id}][value]",
                  'value' => $extra->value
                ];
            $repeatersFields[] = [
                    'name'  => "blocks[extra-{$extra->id}][field]",
                    'value' => $extra->field
                ];

            $repeaters[] = [
                'id' => 'extra' . '-' . $extra->id,
                'type' => $repeatersList['extra']['component'],
                'title' => $repeatersList['extra']['title'],
            ];
        }

        $fields['repeaters']['extra'] = $repeaters;
        $fields['repeaterFields']['extra'] = $repeatersFields;

        return $fields;
    }


    // implement the afterSave method
    public function afterSave($object, $fields)
    {

        // save creators with the associated roles
        if (array_key_exists('repeaters', $fields)) {
            $this->saveRepeaterWithPivot($fields['repeaters']['people'] ?? [], $object);

            // sync hasMany relations for extras
            $object->extras()->delete();
            $object->extras()->createMany($fields['repeaters']['extra'] ?? []);

        }

        $this->updateBrowser($object, $fields, 'languages');
        $this->updateBrowser($object, $fields, 'countries');


        parent::afterSave($object, $fields);
    }

    /**
     * @param $repeater
     * @param Model $object
     */
    public function saveRepeaterWithPivot($repeater, $object): void
    {
        $relationFields = $repeater ?? [];
        $relationRepository = $this->getModelRepository('people', 'Creator');

        $attaches = [];
        foreach ($relationFields as $relationField) {
            $newRelation = $relationRepository->firstOrCreate(Arr::only($relationField, ['first_name', 'last_name']));
            $attaches[$newRelation->id] = ['role' => $relationField['role']];
        }

        $object->people()->sync($attaches);
    }


}
