<?php

namespace App\Repositories;


use A17\Twill\Repositories\ModuleRepository;
use App\Models\Country;

class CountryRepository extends ModuleRepository
{

    public function __construct(Country $model)
    {
        $this->model = $model;
    }
}
