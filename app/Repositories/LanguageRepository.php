<?php

namespace App\Repositories;


use A17\Twill\Repositories\ModuleRepository;
use App\Models\Language;

class LanguageRepository extends ModuleRepository
{


    public function __construct(Language $model)
    {
        $this->model = $model;
    }

}
