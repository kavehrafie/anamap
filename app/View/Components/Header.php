<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Header extends Component
{

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        $menu = [
            [
                'title' => 'Articles',
                'route' => 'blogs.index',
            ],
            [
                'title' => 'Database',
                'route' => 'pop-cultures.index',
            ],
            [
                'title' => 'About',
                'route' => 'about'
            ]
        ];

        return view('components.header',[
            'title' => 'Ancient Americas, Appropriated',
            'subTitle' => 'Modern Representations of the Pre-Columbian Past',
            'menuItems' => $menu,
        ]);
    }
}
