<?php

namespace App\Http\Livewire;

use A17\Twill\Repositories\SettingRepository;
use App\Models\Country;
use App\Models\Language;
use App\Models\PopCulture;
use Livewire\Component;
use Livewire\WithPagination;

class PopIndex extends Component
{
    use WithPagination;

    public $pops = [];
    public $selectedMedias = [];
    public $selectedMediasParents = [];
    public $selectedYears;
    public $selectedLanguages = [];
    public $selectedCountries = [];


    public function resetYears()
    {
        $this->selectedYears = null;
        $this->pops();
    }

    public function dropCountry($country)
    {
        $this->selectedCountries = collect($this->selectedCountries)->except([$country])->values();
        $this->pops();
    }

    public function dropMedia($media)
    {
        $this->selectedMedias = collect($this->selectedMedias)->except([$media])->values();
        $this->pops();
    }

    public function dropLanguage($language)
    {
        $this->selectedLanguages = collect($this->selectedLanguages)->except([$language])->values();
        $this->pops();
    }

    public function updated($name, $value)
    {
        $this->pops();
    }

    public function selectMediasParents($media)
    {
        if (in_array($media, $this->selectedMediasParents)) {

            foreach ($this->mediaOptions()[$media] as $item) {
                if (array_search($item, $this->selectedMedias) === false) {
                    $this->selectedMedias[] = $item;
                }
            }

        } else {
            // TODO refract this code
            foreach ($this->mediaOptions()[$media] as $item) {
                $index = array_search($item, $this->selectedMedias);
                unset($this->selectedMedias[$index]);

            }
        }
    }

    public function selectMediasChild($parent)
    {
        foreach ($this->mediaOptions()[$parent] as $item) {
            if (array_search($item, $this->selectedMedias) === false) {
                $index = array_search($parent,  $this->selectedMediasParents);
                unset($this->selectedMediasParents[$index]);
                return;
            }
        }
        $this->selectedMediasParents[] = $parent;

    }
    public function pops()
    {
        $this->pops = PopCulture::filter([
            'years' => $this->selectedYears,
            'countries' => $this->selectedCountries,
            'languages' => $this->selectedLanguages,
            'medias' => $this->selectedMedias
        ])->published()->get();
    }

    public function mount()
    {
        $this->pops = PopCulture::published()->get();
    }

    public function render()
    {
        $currYear = now()->year;

        return view('livewire.pop-index', [
            'min' => 1800,
            'max' => $currYear,
            'types' => $this->mediaOptions(),
            'languages' => Language::has('pops')->get()->pluck('name', 'id'),
            'countries' => Country::has('pops')->get()->pluck('name', 'id'),
            'intervals' => [
                '1800 - 1900' => [1800, 1900],
                '1901 - 1950' => [1901, 1950],
                '1951 - 2000' => [1951, 2000],
                '2001 - now' => [2001, $currYear]
            ]
        ]);
    }

    private function mediaOptions()
    {
        $mediaOptions = [];
        foreach (explode(', ',  app(SettingRepository::class)->byKey('media_types')) ?? [] as $media) {
            if (strpos($media, '.')) {
                $str = explode('.', $media);
                $mediaOptions[$str[0]][] = strtolower($str[1]);
            }
            else {
                $mediaOptions[] =  ucfirst($media);
            }
        }
        return $mediaOptions;
    }
}
