<?php

namespace App\Http\Livewire;

use Livewire\Component;

class AppliedFilters extends Component
{
    public $filters;

    protected $listeners = ['popsSetFilters' => 'setFilters', 'resetYears'];

    public function mount()
    {
        $this->filters = [];
    }

    public function setFilters($filters)
    {
        $this->filters = $filters;
    }

    public function resetYears()
    {
        $this->emit('popsSetFilters', collect($this->filters)->except(['years']));
    }

    public function render()
    {
        return view('livewire.applied-filters');
    }
}
