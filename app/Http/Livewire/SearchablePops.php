<?php

namespace App\Http\Livewire;

use App\Models\PopCulture;
use App\Repositories\PopCultureRepository;
use Livewire\Component;

class SearchablePops extends Component
{
    public $pops = [];

    public $filters;

    protected $listeners = [
        'setFilters',
    ];

    public function setFilters($filters)
    {
//        dd($this->filters);
//        $this->filters['years'] = $filters;
        dd($filters);
        $this->refresh();
    }


    public function refresh()
    {
        $this->pops = PopCulture::filter($this->filters)->published()->get();
    }

    public function mount()
    {
//        $this->filters = [];
//        $this->pops = $repository->get(['people', 'countries', 'languages'], ['published' => true], [],-1);
        $this->pops = PopCulture::published()->get();
    }

    public function render()
    {
        return view('livewire.searchable-pops');
    }
}
