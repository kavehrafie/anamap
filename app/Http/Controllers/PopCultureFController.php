<?php

namespace App\Http\Controllers;

use A17\Twill\Repositories\SettingRepository;
use App\Repositories\PopCultureRepository;
use Illuminate\Http\Request;

class PopCultureFController extends Controller
{
    protected $repository;
    public function __construct(PopCultureRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        return view('pages.pop_cultures.index');
    }
}
