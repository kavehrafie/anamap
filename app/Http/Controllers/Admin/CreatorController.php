<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class CreatorController extends ModuleController
{
    protected $moduleName = 'creators';

    protected $titleFormKey = 'full_name';

    protected $titleColumnKey = 'full_name';

    protected $indexColumns = [
        'full_name' => [
            'title' => 'Full name',
            'field' => 'full_name'
        ]
    ];

    protected $browserColumns = [
        'full_name' => [
            'title' => 'Full name',
            'field' => 'full_name'
        ]
    ];

}
