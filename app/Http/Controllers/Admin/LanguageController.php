<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class LanguageController extends ModuleController
{
    protected $moduleName = 'languages';

    protected $titleFormKey = 'name';

    protected $titleColumnKey = 'name';

    protected $indexColumns = [
        'name' => [
            'title' => 'Name',
            'field' => 'name'
        ]
    ];

    protected $browserColumns = [
        'name' => [
            'title' => 'Name',
            'field' => 'name'
        ]
    ];

    protected $defaultOrders = ['name' => 'asc'];

    protected function setMiddlewarePermission()
    {
        $this->middleware('can:list', ['only' => ['index', 'show']]);
    }

}
