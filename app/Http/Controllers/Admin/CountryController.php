<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class CountryController extends ModuleController
{
    protected $moduleName = 'countries';

    protected $titleFormKey = 'name';

    protected $titleColumnKey = 'name';

    protected $indexColumns = [
        'name' => [
            'title' => 'Name',
            'field' => 'name'
        ]
    ];

    protected $browserColumns = [
        'name' => [
            'title' => 'Name',
            'field' => 'name'
        ]
    ];

    protected $defaultOrders = ['name' => 'asc'];
}
