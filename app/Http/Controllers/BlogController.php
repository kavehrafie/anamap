<?php

namespace App\Http\Controllers;

use App\Repositories\BlogRepository;
use Illuminate\Http\Request;

class BlogController extends Controller
{

    public function __construct(BlogRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.blogs.index', [
            'articles' => $this->repository->get([],[],['created_at' => 'desc'])
        ]);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $article = $this->repository->forSlug($slug);
//        dd($this->repository->next($article->position), $this->repository->previous($article->position));
        return view('pages.blogs.show', [
            'article' => $article,
            'next' => $this->repository->next($article->position),
            'previous' => $this->repository->previous($article->position)
        ]);
    }

}
