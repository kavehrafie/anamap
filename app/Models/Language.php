<?php

namespace App\Models;



use A17\Twill\Models\Behaviors\IsTranslatable;
use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    use IsTranslatable;
    protected $fillable = [
        'published',
        'title',
        'description',
    ];

    public function pops()
    {
        return $this->belongsToMany(PopCulture::class);
    }
}
