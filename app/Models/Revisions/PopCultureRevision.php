<?php

namespace App\Models\Revisions;

use A17\Twill\Models\Revision;

class PopCultureRevision extends Revision
{
    protected $table = "pop_culture_revisions";
}
