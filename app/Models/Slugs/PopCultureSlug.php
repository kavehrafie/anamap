<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class PopCultureSlug extends Model
{
    protected $table = "pop_culture_slugs";
}
