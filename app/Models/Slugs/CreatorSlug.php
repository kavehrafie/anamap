<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class CreatorSlug extends Model
{
    protected $table = "creator_slugs";
}
