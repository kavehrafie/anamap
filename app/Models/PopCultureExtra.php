<?php

namespace App\Models;



use Illuminate\Database\Eloquent\Model;

class PopCultureExtra extends Model
{
    protected $table = 'pop_culture_extras';

    protected $fillable = [
        'field',
        'value'
    ];
}
