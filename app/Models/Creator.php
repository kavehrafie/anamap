<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Model;

class Creator extends Model
{
    use HasSlug, HasMedias;

    protected $fillable = [
        'published',
        'first_name',
        'last_name',
    ];

    protected $appends = ['full_name'];

    public $slugAttributes = [
        'full_name',
    ];

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public $mediasParams = [
        'cover' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 1,
                ],
            ],
        ],
    ];
}
