<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\IsTranslatable;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use IsTranslatable;
    protected $fillable = [
        'name',
        'code'
    ];

    public $timestamps = false;

    public function pops()
    {
        return $this->belongsToMany(PopCulture::class);
    }
}
