<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasBlocks;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Behaviors\HasRevisions;
use A17\Twill\Models\Model;
use Cartalyst\Tags\TaggableTrait;
use Illuminate\Database\Eloquent\Builder;

class PopCulture extends Model
{
    use HasBlocks, HasSlug, HasMedias, HasRevisions, TaggableTrait;


    protected $fillable = [
        'published',
        'title',
        'description',
        'start_year',
        'end_year',
        'media_type',
        'country',
        'language',
        'notes',
        'publisher',
        'title_in_series',
        'number_in_series'
    ];

    public $slugAttributes = [
        'title',
    ];


    public $mediasParams = [
        'slides' => [
            'flexible' => [
                [
                    'name' => 'free',
                    'ratio' => 0,
                ],
                [
                    'name' => 'landscape',
                    'ratio' => 16 / 9,
                ],
                [
                    'name' => 'portrait',
                    'ratio' => 3 / 5,
                ],
            ],
        ],
        'cover' => [
            'default' => [
                [
                    'name' => 'default',
                    'ratio' => 1,
                ],
            ],
            'free' => [
                [
                    'name' => 'free',
                    'ratio' => 0,
                ],
            ],
        ],
    ];


    public function countries()
    {
        return $this->belongsToMany(Country::class);
    }

    public function languages()
    {
        return $this->belongsToMany(Language::class);
    }

    public function people()
    {
        return $this->belongsToMany(Creator::class)->withPivot('role');
    }

    public function extras()
    {
        return $this->hasMany(PopCultureExtra::class);
    }

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['years'] ?? null, function ($query, $years) {
            $tmp = array_map('intval', $years);
            sort($tmp);
            $query->whereBetween('start_year', $tmp);
        })->when($filters['countries'] ?? null, function ($query, $countries) {
            if(count($countries)) {
                $query->whereHas('countries', function (Builder $query) use ($countries) {
                    $query->whereIn('name', $countries);
                });
            }
        })->when($filters['languages'] ?? null, function ($query, $languages) {
            if(count($languages)) {
                $query->whereHas('languages', function (Builder $query) use ($languages) {
                    $query->whereIn('name', $languages);
                });
            }
        })->when($filters['medias'] ?? null, function ($query, $medias) {
            if(count($medias)) {
                $query->whereIn('media_type', $medias);
            }
        });
    }
}
